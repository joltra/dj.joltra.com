var express = require('express');
var app = exports.app = express();
//https://github.com/mashpie/i18n-node
var i18n = require("i18n");
i18n.configure({
    locales:['es', 'en'],
    directory: __dirname + '/locales',
    extension: '.js',
    defaultLocale: "en",
    updateFiles: true
});
//var greeting = i18n.__('Hello');

var debug = false;

var path = require ('path');
var serveStatic = require('serve-static');
app.use(serveStatic(path.join(__dirname + '/public')));
var bodyParser = require('body-parser');
app.use(bodyParser());       // to support JSON-encoded or URL-encoded bodies

//use swig as render engine
var swig = require('swig');

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

// init i18n module for this loop
app.use(i18n.init);

app.use(function (req, res, next) {
	// mustache helper
	res.locals.__ = function () {
	  return function (text, render) {
	    return i18n.__.apply(req, arguments);
	  };
	};

	next();
});

app.use(function (req, res, next) {
	switch (req.headers.host) {
		case "en.joltra.com":
			i18n.setLocale("en");
			break;
		default:
			i18n.setLocale("es");
	}
	next();
});

var cookieParser = require('cookie-parser');
var session      = require('express-session');

app.use(cookieParser());
app.use(session({secret: "sercretorrrl", name: 'sid', proxy: true}));

// Swig will cache templates for you, but you can disable
// that and use Express's caching instead, if you like:
app.set('view cache', false);
// To disable Swig's cache, do the following:
swig.setDefaults({ cache: false });
// NOTE: You should always cache templates in a production environment.
// Don't leave both of these to `false` in production!

//swig i18n filter
swig.setFilter('trans', function(input){
	console.log('trans filter locale: ' + i18n.getLocale());
	return i18n.__(input);
});

//swig i18n trans tag
swig.setExtension('trans', function(text){
	return i18n.__(text);
});

var TransTag = {
	Parse: function (str, line, parser, types, options) {
		return true;
	},
	Compile: function (compiler, args, content, parents, options, blockName) {
		return '_output += _ext.trans("' + content + '");';
	},
	ends: true,
	blockLevel: true
};

swig.setTag("trans", TransTag.Parse, TransTag.Compile, TransTag.ends, TransTag.blockLevel);

/*var sdk = require("./sdk");
sdk.Init("127.0.0.1", 5984, "admin", "CC1p5ll4a", "browserdj", function(result){
	sdk.Users.debug = debug;
	sdk.Articles.debug = debug;
	sdk.Comments.debug = debug;
	sdk.Messages.debug = debug;

	require("./api")(app, sdk, debug);
});*/
require("./api")(app, null, debug);