$(function(){
	$('#divSongs').on('dragover',
	    function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	    }
	);
	$('#divSongs').on('dragenter',
	    function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	    }
	);
	$('#divSongs').on('drop',
	    function(e){
	        if(e.originalEvent.dataTransfer){
	            if(e.originalEvent.dataTransfer.files.length) {
	                e.preventDefault();
	                e.stopPropagation();
	                
	                var droppedFiles = e.originalEvent.dataTransfer.files;

	                for (var i = 0; i < droppedFiles.length; i++){
	                	var reader = new FileReader();

					    reader.onload = function(j) { return function(e) {
							// get file content
							LoadMP3(e.target.result, droppedFiles[j]);
					    }; }(i);

					    reader.readAsArrayBuffer(e.originalEvent.dataTransfer.files[i]);
	                }
	                
	            }   
	        }
	    }
	);

	$("#btnStopAll").on('click', function(event){
		for (var i = 0; i < sources.length; i++)
			if(sources[i] != null)
				sources[i].stop();

		sources = [];
	});

	$("#btnStop1").on('click', function(event){
		sources[0].stop();

		sources[0] = null;
	});

	$("#btnStop2").on('click', function(event){
		sources[1].stop();

		sources[1] = null;
	});

	$("#btnPlay1").on('click', function(event){
		playSong(deckSong[0], 0);
	});

	$("#btnPlay2").on('click', function(event){
		playSong(deckSong[1], 1);
	});

	$("#sldPitch1").on('change', function(event){
		if(sources[0] != null)
			sources[0].playbackRate.value = $("#sldPitch1").val();
	});

	$("#sldPitch2").on('change', function(event){
		if(sources[1] != null)
			sources[1].playbackRate.value = $("#sldPitch2").val();
	});

	$("#sldFilFreqLow1").on('change', function(event){
		if(filLows[0] != null)
			filLows[0].frequency.value = $("#sldFilFreqLow1").val();
	});

	$("#sldFilFreqLow2").on('change', function(event){
		if(filLows[1] != null)
			filLows[1].frequency.value = $("#sldFilFreqLow2").val();
	});

	$("#sldVolume1").on('change', function(event){
		if(gains[0] != null)
			gains[0].gain.value = $("#sldVolume1").val();
	});

	$("#sldVolume2").on('change', function(event){
		if(gains[1] != null)
			gains[1].gain.value = $("#sldVolume2").val();
	});

	initAudio();
});

var context;
var files = [];
var songs = [];
var deckSong = [];
function initAudio() {
  try {
    // Fix up for prefixing
    window.AudioContext = window.AudioContext||window.webkitAudioContext;
    context = new AudioContext();

    offlineContext = new OfflineAudioContext();
  }
  catch(e) {
    alert('Web Audio API is not supported in this browser');
  }
}

var currentDeck = 0;

function LoadMP3(data, file){
	context.decodeAudioData(data, function(buffer) {
      songs[songs.length] = buffer;
      files[songs.length - 1] = file;
      $("#divSongs").append("<button class='btn btn-default' id='btnSong" + songs.length + "'><i class='fa fa-music'></i> " + file.name.replace(".mp3", "") + "</button>");
      $("#btnSong" + songs.length).on("click", function(j) { 
      	return function(event){
      		deckSong[currentDeck] = j;
      		$("#txtSongTitle" + (currentDeck + 1)).html(files[j].name.replace(".mp3", ""));

      		currentDeck = (currentDeck + 1) % 2;
      	};
      }(songs.length - 1));
      //playSong(songs.length);
    }, function(error){
    	alert(JSON.stringify(error));
    });
}

var sources = [];
var filLows = [];
var gains = [];

function playSong(index, deck) {
  sources[deck] = context.createBufferSource(); // creates a sound source
  sources[deck].buffer = songs[index];              // tell the source which sound to play
  sources[deck].playbackRate.value = $("#sldPitch" + (deck + 1)).val()

  // Create the low pass filter
  filLows[deck] = context.createBiquadFilter();

  filLows[deck].type = 0; // Low-pass filter. See BiquadFilterNode docs
  var minValue = 40;
  var maxValue = context.sampleRate / 2;
  $("#sldFilFreqLow" + (deck + 1)).attr("max", maxValue);
  $("#sldFilFreqLow" + (deck + 1)).val(maxValue);
  filLows[deck].frequency.value = maxValue; // Set cutoff to 440 HZ

  sources[deck].connect(filLows[deck]);

  //create the gain node to control volume
  gains[deck] = context.createGainNode();

  filLows[deck].connect(gains[deck]);       // connect the source to the context's destination (the speakers)
  gains[deck].connect(context.destination);

  sources[deck].start(0);                           // play the source now
}

var offlineContext;
var scriptNode;

function extractBPM(song){
	var source = offlineContext.createBufferSource(); // creates a sound source
	source.buffer = song;

	// setup a javascript node
	scriptNode = offlineContext.createScriptProcessor(2048, 1, 1);
	// connect to destination, else it isn't called
	scriptNode.connect(offlineContext.destination);

	// setup a analyzer
	analyser = offlineContext.createAnalyser();
	analyser.smoothingTimeConstant = 0.3;
	analyser.fftSize = 1024;

	analyser2 = offlineContext.createAnalyser();
	analyser2.smoothingTimeConstant = 0.0;
	analyser2.fftSize = 1024;

	// create a buffer source node
	sourceNode = offlineContext.createBufferSource();
	splitter = offlineContext.createChannelSplitter();

	// connect the source to the analyser and the splitter
	sourceNode.connect(splitter);

	// connect one of the outputs from the splitter to
	// the analyser
	splitter.connect(analyser,0,0);
	splitter.connect(analyser2,1,0);

	// we use the javascript node to draw at a
	// specific interval.
	analyser.connect(scriptNode);

	// and connect to destination
	sourceNode.connect(offlineContext.destination);
}

scriptNode.onaudioprocess = function() {
    // get the average for the first channel
    var array =  new Uint8Array(analyser.frequencyBinCount);
    analyser.getByteFrequencyData(array);
    var average = getAverageVolume(array);

    // get the average for the second channel
    var array2 =  new Uint8Array(analyser2.frequencyBinCount);
    analyser2.getByteFrequencyData(array2);
    var average2 = getAverageVolume(array2);

    // clear the current state
    ctx.clearRect(0, 0, 60, 130);

    // set the fill style
    ctx.fillStyle=gradient;

    // create the meters
    ctx.fillRect(0,130-average,25,130);
    ctx.fillRect(30,130-average2,25,130);
}

function getAverageVolume(array) {
	var values = 0;
	var average;

	var length = array.length;

	// get all the frequency amplitudes
	for (var i = 0; i < length; i++) {
		values += array[i];
	}

	average = values / length;
	return average;
}