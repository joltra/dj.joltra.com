	var debugApi = false;

module.exports = function (app, sdk, debug) {
	debugApi = debug;

	app.get('/', function(req, res){
		if(debugApi) console.log('/ called, locale: ' + req.locale);
		if(!req.session.user) req.session.user = { name:"Guest", level: "guest" };

		res.render('main', { user: req.session.user } );
	});
}