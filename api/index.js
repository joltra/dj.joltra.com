var apiroot = "/api";
var debugApi = false;

module.exports = function (app, sdk, debug) {
    debugApi = debug;

    //forms
    require("./forms.js")(app, sdk, debug);

    //api
    /*require("./articles.js")(app, sdk, debug);
    require("./comments.js")(app, sdk, debug);
    require("./messages.js")(app, sdk, debug);*/

    //login
    app.post(apiroot + '/login', function (req, res) {
        sdk.Users.GetByEmail(req.body.email, function (result) {
            var resultData = { success: 0, data: "Incorrect user and/or password." };

            if (result.success == 1) {
                if (result.data.length > 0) {
                    var user = result.data[0];
                    
                    if(req.body.password == user.password){
                        req.session.user = user;
                        resultData = { success: 1, data: { name: user.name }};
                    }
                }
            }

            res.send(resultData);
        });
    });

    //login
    app.post(apiroot + '/register', function (req, res) {
        sdk.Users.GetByEmail(req.body.email, function (result) {
            if(result.success == 1){
                if(result.length == 0){
                    // Insert: function (email, name, pass, level, callback)
                    sdk.Users.Insert(req.body.email, req.body.name, req.body.pass, "user", function (result) {
                        var resultData = { success: 0, data: "Incorrect user and/or password." };

                        if (result.success == 1) {
                            var user = result.data;
                                
                            req.session.user = user;
                            resultData = { success: 1, data: { name: user.name }};
                        }

                        res.send(resultData);
                    });
                }else{
                    res.send({ success: 0, data: "User already exists." });
                }
            }else{
                res.send({ success: 0, data: "Unexpected error." });
            }
        });
    });

    //install
    app.post(apiroot + '/install', function(req, res){
        if(!req.session.user) req.session.user = { name:"Guest", level: "guest" };
        sdk.Users.GetAll(function(result){
            var admin = false;
            if(result.success == 1){
                for(var i = 0; i < result.data.length; i++){
                    if(result.data[i].level == "admin"){
                        admin = true;
                        break;
                    }
                }
            }
            if(admin)
                res.send({ success: 0, data: "Can't install" });
            else {
                //Insert: function (email, name, pass, level, callback)
                sdk.Users.Insert(req.body.email, req.body.name, req.body.password, "admin", function(result){
                    if(result.success == 1){
                        res.send({ success: 1, data: { name: result.data.name }})
                    }else{
                        res.send({ success: 0, data: "Can't install" });
                    }
                });
            }
        });
    });//api/install
}